package com.example.itp4501assignment.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.itp4501assignment.Model.Question;
import com.example.itp4501assignment.Model.Questions;
import com.example.itp4501assignment.Model.TestLog;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {

    private final static int _DBVersion = 4;
    private final static String _DBName = "Log.db";

    public DBHelper(Context context) {
        super(context, _DBName, null, _DBVersion);
    }

    public boolean insertTestLog(TestLog testLog) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(LogContract.TestLog.COLUMN_NAME_SECOND, testLog.seconds);
            values.put(LogContract.TestLog.COLUMN_NAME_AVERAGE, testLog.average);
            values.put(LogContract.TestLog.COLUMN_NAME_CORRECT, testLog.correct);
            long newRowId = db.insert(LogContract.TestLog.TABLE_NAME, null, values);

            for(Question q:testLog.questions.questions){
                values = new ContentValues();
                values.put(LogContract.QuestionLog.COLUMN_NAME_TESTLOGID, newRowId);
                values.put(LogContract.QuestionLog.COLUMN_NAME_QUESTION, q.question);
                values.put(LogContract.QuestionLog.COLUMN_NAME_ANSWER,String.valueOf(q.answer));
                db.insert(LogContract.QuestionLog.TABLE_NAME, null, values);
            }
            System.out.println("insert");
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public ArrayList<TestLog> getTestLog() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM testlog As t ORDER BY t._id DESC LIMIT 10 ",null);
        ArrayList<TestLog> testLogs = new ArrayList<>();
        while(cursor.moveToNext()) {
            TestLog testLog = new TestLog();
            testLog.id = cursor.getInt(cursor.getColumnIndex(LogContract.TestLog._ID));
            testLog.seconds = cursor.getString(cursor.getColumnIndex(LogContract.TestLog.COLUMN_NAME_SECOND));
            testLog.average = cursor.getString(cursor.getColumnIndex(LogContract.TestLog.COLUMN_NAME_AVERAGE));
            testLog.correct = cursor.getInt(cursor.getColumnIndex(LogContract.TestLog.COLUMN_NAME_CORRECT));
            String[] args = {testLog.id+""};
            Cursor cursor1 = db.rawQuery("SELECT * FROM questionlog WHERE testlogid = ?",args);
            ArrayList<Question> questions = new ArrayList<>();
            while(cursor1.moveToNext()) {
                Question question = new Question();
                question.question = cursor1.getString(cursor1.getColumnIndex(LogContract.QuestionLog.COLUMN_NAME_QUESTION));
                question.answer = Integer.parseInt(cursor1.getString(cursor1.getColumnIndex(LogContract.QuestionLog.COLUMN_NAME_ANSWER)));
                questions.add(question);
            }
            testLog.questions = new Questions(questions.toArray(new Question[questions.size()]));
            testLogs.add(testLog);
        }
        cursor.close();
        return testLogs;
    }
    public ArrayList<TestLog> getTestLogCorrect() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT correct FROM testlog As t ORDER BY t._id DESC LIMIT 10 ",null);
        ArrayList<TestLog> testLogs = new ArrayList<>();
        while(cursor.moveToNext()) {
            TestLog testLog = new TestLog();
            testLog.correct = cursor.getInt(cursor.getColumnIndex(LogContract.TestLog.COLUMN_NAME_CORRECT));
            testLogs.add(testLog);
        }
        cursor.close();
        return testLogs;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LogContract.QuestionLog.SQL_CREATE_ENTRIES);
        db.execSQL(LogContract.TestLog.SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE questionlog");
        db.execSQL("DROP TABLE testlog");
        db.execSQL(LogContract.QuestionLog.SQL_CREATE_ENTRIES);
        db.execSQL(LogContract.TestLog.SQL_CREATE_ENTRIES);
    }
}
