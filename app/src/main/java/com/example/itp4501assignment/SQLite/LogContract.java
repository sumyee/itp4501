package com.example.itp4501assignment.SQLite;

import android.provider.BaseColumns;

public class LogContract {
    private LogContract() {}

    public static class QuestionLog implements BaseColumns {
        public static final String TABLE_NAME = "questionlog";
        public static final String COLUMN_NAME_QUESTION = "question";
        public static final String COLUMN_NAME_ANSWER = "answer";
        public static final String COLUMN_NAME_TESTLOGID = "testlogid";
        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        COLUMN_NAME_TESTLOGID + " INTEGER,"+
                        COLUMN_NAME_QUESTION + " TEXT," +
                        COLUMN_NAME_ANSWER + " TEXT)";
    }
    public static class TestLog implements BaseColumns {
        public static final String TABLE_NAME = "testlog";
        public static final String COLUMN_NAME_SECOND = "second";
        public static final String COLUMN_NAME_AVERAGE = "average";
        public static final String COLUMN_NAME_CORRECT = "correct";
        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        COLUMN_NAME_SECOND + " TEXT," +
                        COLUMN_NAME_AVERAGE + " TEXT," +
                        COLUMN_NAME_CORRECT + " INTEGER)";
    }
}
