package com.example.itp4501assignment.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.example.itp4501assignment.Fragment.StatisticView;
import com.example.itp4501assignment.Model.TestLog;
import com.example.itp4501assignment.R;
import com.example.itp4501assignment.SQLite.DBHelper;

import java.util.ArrayList;

public class StatisticActivity extends AppCompatActivity {

    private LinearLayout layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);

        layout =  findViewById(R.id.linearLayout1);
        DBHelper dbHelper = new DBHelper(this);
        ArrayList<TestLog> testlogs = dbHelper.getTestLogCorrect();
        StatisticView alarmView = new StatisticView(this,testlogs);
        layout.addView(alarmView);
    }
}
