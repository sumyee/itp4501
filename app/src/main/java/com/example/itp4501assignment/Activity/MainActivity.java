package com.example.itp4501assignment.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.itp4501assignment.Model.Question;
import com.example.itp4501assignment.Model.Questions;
import com.example.itp4501assignment.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnStart, btnRecord,btnChart;
    private ProgressDialog progressDialog;
    private static final String url = "https://ajtdbwbzhh.execute-api.us-east-1.amazonaws.com/default/201920ITP4501Assignment";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view();


    }
    @Override
    public void onClick(View v) {
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Questions questions = getQuestions();
                    List<Question> qs = Arrays.asList(questions.questions);
                    Collections.shuffle(qs);
                    qs = qs.subList(0,5);
                    questions.questions = qs.toArray(new Question[5]);
                    Intent intent=new Intent(MainActivity.this,QuestionActivity.class);

                    intent.putExtra("questions",questions);
                    startActivity(intent);
                }catch (Exception e){
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,"fail to download questions",Toast.LENGTH_SHORT).show();
                        }
                    });
                }finally {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                }
            }
        }).start();

    }

    private Questions getQuestions() throws Exception{
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            return new Gson().fromJson(response.body().string(),Questions.class);
        }
    }
    private void view(){
        btnStart = findViewById(R.id.btnStart);
        btnRecord = findViewById(R.id.btnRecord);
        btnChart = findViewById(R.id.btnChart);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Downloading Questions");
        progressDialog.setIndeterminate(false);

        btnStart.setOnClickListener(this);
        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,RecordActivity.class);
                startActivity(intent);
            }
        });
        btnChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,StatisticActivity.class);
                startActivity(intent);
            }
        });
    }


}
