package com.example.itp4501assignment.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.itp4501assignment.Fragment.OnAnswerSelectListener;
import com.example.itp4501assignment.Fragment.QuestionFragment;
import com.example.itp4501assignment.Model.Questions;
import com.example.itp4501assignment.R;

import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

public class QuestionActivity extends AppCompatActivity implements OnAnswerSelectListener {

    private Questions questions;
    private ViewPager vpQuestions;
    private PagerAdapter pagerAdapter;
    private ProgressBar progressBar;
    private TextView txtTimer;
    private int correct = 0;
    private int progress = 1;
    private long startTime;

private Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        vpQuestions = findViewById(R.id.vpQuestions);
        progressBar = findViewById(R.id.progressBar);
        txtTimer = findViewById(R.id.txtTimer);

        progressBar.setMax(5);
        progressBar.setProgress(progress);
        Intent intent = getIntent();
        questions = (Questions) intent.getSerializableExtra("questions");
        pagerAdapter = new QuestionPagerAdapter(getSupportFragmentManager());
        vpQuestions.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        vpQuestions.setAdapter(pagerAdapter);
        startTime = System.currentTimeMillis();
        timer = new Timer(true);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                updateTimer();
            }
        },100,50);
    }

    private void updateTimer(){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                double s = (System.currentTimeMillis() - startTime)/1000d;
                if(s>60){
                    txtTimer.setText(String.format("%d m %.1f s",(int)(s/60),(int)s%60));
                }else{
                    txtTimer.setText(String.format("%.1f s",s));
                }

            }
        });

    }
    @Override
    public void onAnswerSelect(boolean isCorrect) {
        correct += isCorrect ? 1 : 0;
        vpQuestions.setCurrentItem(progress);
        progressBar.setProgress(++progress);
        if (progress > 5) {

            Intent intent = new Intent(this, ResultActivity.class);
            intent.putExtra("time", (System.currentTimeMillis() - startTime));
            intent.putExtra("correct", correct);
            intent.putExtra("questions", questions);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onDestroy() {
        timer.cancel();
        super.onDestroy();

    }

    private class QuestionPagerAdapter extends FragmentStatePagerAdapter {
        public QuestionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new QuestionFragment(questions.questions[position], QuestionActivity.this);
        }

        @Override
        public int getCount() {
            return 5;
        }
    }
}
