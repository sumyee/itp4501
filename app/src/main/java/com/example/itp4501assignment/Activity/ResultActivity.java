package com.example.itp4501assignment.Activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.itp4501assignment.Model.Questions;
import com.example.itp4501assignment.Model.TestLog;
import com.example.itp4501assignment.R;
import com.example.itp4501assignment.SQLite.DBHelper;

public class ResultActivity extends AppCompatActivity {

    private TextView txtTimeSpend, txtAvgTimeSpend, txtCorrect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();
        int correct = intent.getIntExtra("correct", 0);
        long time = intent.getLongExtra("time", 0);
        Questions questions = (Questions) intent.getSerializableExtra("questions");
        TestLog testLog = new TestLog(String.format("%.1f",time / 1000d),String.format("%.1f",time/5000d),correct,questions);


        txtTimeSpend = findViewById(R.id.txtTimeSpend);
        txtAvgTimeSpend = findViewById(R.id.txtAvgTimeSpend);
        txtCorrect = findViewById(R.id.txtCorrect);

        txtTimeSpend.setText(String.format("%s%s", txtTimeSpend.getText().toString(), testLog.seconds));
        txtAvgTimeSpend.setText(String.format("%s%ss", txtAvgTimeSpend.getText().toString(), testLog.average));
        txtCorrect.setText(String.format("%s%d", txtCorrect.getText().toString(), testLog.correct));

        DBHelper dbHelper = new DBHelper(this);
        dbHelper.insertTestLog(testLog);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("");
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
