package com.example.itp4501assignment.Activity;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.ActionBar;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.example.itp4501assignment.Model.Question;
import com.example.itp4501assignment.Model.TestLog;
import com.example.itp4501assignment.R;
import com.example.itp4501assignment.SQLite.DBHelper;
import com.example.itp4501assignment.SQLite.LogContract;

import java.util.ArrayList;

public class RecordActivity extends AppCompatActivity implements ExpandableListView.OnGroupExpandListener, Toolbar.OnMenuItemClickListener {

    private ArrayList<TestLog> testlogs;
    private ExpandableListView expandableListView;
    private ExpandableListAdapter expandableListAdapter;
    private int lastExpand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Record");

        expandableListView = findViewById(R.id.expandableListView);
        DBHelper dbHelper = new DBHelper(this);
        testlogs = dbHelper.getTestLog();
        for (TestLog t : testlogs) {
            System.out.println(t);
        }
        expandableListAdapter = new RecordExpandableAdapter();
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onGroupExpand(int groupPosition) {
        if (groupPosition != lastExpand) {
            expandableListView.collapseGroup(lastExpand);
            lastExpand = groupPosition;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        return true;
    }

    class RecordExpandableAdapter extends BaseExpandableListAdapter {

        public RecordExpandableAdapter() {

        }

        @Override
        public int getGroupCount() {
            return testlogs.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return testlogs.get(groupPosition).questions.questions.length;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return testlogs.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return testlogs.get(groupPosition).questions.questions[childPosition];
        }

        @Override
        public long getGroupId(int groupPosition) {
            return testlogs.get(groupPosition).id;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return groupPosition * childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            TestLog listTitle = (TestLog) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) RecordActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.lst_group, null);
            }
            TextView txtTimeSpend = convertView.findViewById(R.id.txtTimeSpend);
            txtTimeSpend.setText("Time Spend: " + listTitle.seconds + " s");
            TextView txtAvgTime = convertView.findViewById(R.id.txtAvgTimeSpend);
            txtAvgTime.setText("Average Time Spend: " + listTitle.average + " s");
            TextView txtCorrect = convertView.findViewById(R.id.txtCorrect);
            txtCorrect.setText("Correct: " + listTitle.correct + " questions");
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            final Question question = (Question) getChild(groupPosition, childPosition);
            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) RecordActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.lst_item, null);
            }
            TextView txtQuestion = (TextView) convertView.findViewById(R.id.txtQuestion);
            txtQuestion.setText("Question: " + question.question);
            TextView txtAnser = (TextView) convertView.findViewById(R.id.txtAnswer);
            txtAnser.setText("Answer: " + question.answer);
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }
}
