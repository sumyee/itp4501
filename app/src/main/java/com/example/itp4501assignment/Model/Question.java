package com.example.itp4501assignment.Model;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class Question implements Serializable {
    public String question;
    public int answer;

    @NonNull
    @Override
    public String toString() {
        return question + answer;
    }
}
