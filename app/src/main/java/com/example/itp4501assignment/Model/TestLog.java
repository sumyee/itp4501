package com.example.itp4501assignment.Model;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Arrays;

public class TestLog implements Serializable {
    public String seconds;
    public String average;
    public int correct;
    public Questions questions;
    public long id;
    public TestLog(String seconds, String average, int correct, Questions questions) {
        this.seconds = seconds;
        this.average = average;
        this.correct = correct;
        this.questions = questions;
    }
    public TestLog(){

    }

    @NonNull
    @Override
    public String toString() {
        return id+" "+seconds +" "+average+" "+correct+" "+" "+Arrays.toString(questions.questions);
    }
}
