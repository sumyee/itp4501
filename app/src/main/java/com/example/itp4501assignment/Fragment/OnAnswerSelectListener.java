package com.example.itp4501assignment.Fragment;

public interface OnAnswerSelectListener {
    void onAnswerSelect(boolean isCorrect);
}
