package com.example.itp4501assignment.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.itp4501assignment.Model.Question;
import com.example.itp4501assignment.R;


public class QuestionFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {


    private OnAnswerSelectListener mListener;
    private Question question;
    private TextView txtQuestion;
    private RadioButton[] rbs;
    private RadioGroup rgAnswer;
    private Button btnSelect;

    public QuestionFragment() {
    }

    public QuestionFragment(Question question, OnAnswerSelectListener mListener) {
        this.question = question;
        this.mListener = mListener;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_question, container, false);
        txtQuestion = v.findViewById(R.id.txtQuestion);
        rgAnswer = v.findViewById(R.id.rgAnswer);
        rbs = new RadioButton[4];
        rbs[0] = v.findViewById(R.id.rb1);
        rbs[1] = v.findViewById(R.id.rb2);
        rbs[2] = v.findViewById(R.id.rb3);
        rbs[3] = v.findViewById(R.id.rb4);
        btnSelect = v.findViewById(R.id.btnSelect);
        btnSelect.setEnabled(false);
        btnSelect.setOnClickListener(this);
        int ansIndex = (int) (Math.random() * 4);
        for (int i = 0; i < 4; i++) {
            rbs[i].setOnCheckedChangeListener(this);
            rbs[i].setText(i == ansIndex ? question.answer + "" : ((int) (Math.random() * 100) + ""));
        }
        txtQuestion.setText(question.question);

        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        mListener.onAnswerSelect(String.valueOf(question.answer).equals(selectedAns));
    }

    private String selectedAns;

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            btnSelect.setEnabled(true);
            selectedAns = buttonView.getText().toString();
            System.out.println(selectedAns);
            buttonView.setTextColor(Color.rgb(0, 128, 0));
        } else {
            buttonView.setTextColor(Color.BLACK);
        }

    }
}
