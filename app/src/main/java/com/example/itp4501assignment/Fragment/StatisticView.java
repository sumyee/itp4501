package com.example.itp4501assignment.Fragment;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import com.example.itp4501assignment.Model.TestLog;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class StatisticView extends View {

    private static int TOP_MARGIN = 50;
    private static int BOTTOM_MARGIN = 50;
    private static int LEFT_MARGIN = 70;
    private static int RIGHT_MARGIN = 10;

    private static final float FRAME = 100;
    private int[] records;
    private Activity activity;
    private Timer timer;
    private int i = 0;
    private float average;

    public StatisticView(final Activity activity, ArrayList<TestLog> testLogs) {
        super(activity);
        this.activity = activity;


        records = new int[testLogs.size()];

        for (int i = 0; i < records.length; i++) {
            records[i] = testLogs.get(i).correct>5?5:testLogs.get(i).correct;
        }
        average = average();
        timer = new Timer(true);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        StatisticView.this.invalidate();
                    }
                });

            }
        }, 0, 10);

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int fromBottom = canvas.getHeight() - BOTTOM_MARGIN;
        int toTop = TOP_MARGIN + 20;
        int fromLeft = LEFT_MARGIN;
        int toRight = canvas.getWidth() - RIGHT_MARGIN;
        float vRange = (fromBottom - toTop) / FRAME;
        float hRange = (toRight - fromLeft) / (float)(records.length*2);
        float hFrom = fromLeft;
        float vFrom = fromBottom - vRange * i;

        Paint paint = new Paint();
        drawHVLine(canvas, paint, LEFT_MARGIN, RIGHT_MARGIN, BOTTOM_MARGIN, TOP_MARGIN);
        drawScale(canvas, paint, TOP_MARGIN, BOTTOM_MARGIN);

        for (int x = 0; x < records.length * 2; x++) {
            if ((x & 1) == 1) {
                if (records[x / 2] > i / (10 * FRAME / 50)) {
                    drawBar(canvas, paint, fromLeft + x * hRange, hRange, vFrom);
                } else {
                    drawBar(canvas, paint, fromLeft + x * hRange, hRange, toTop + (5 - records[x / 2]) * (fromBottom - toTop) / 5f);
                }
            }

        }
        drawAverage(canvas,toTop,fromBottom);
        i++;
        if (i > FRAME) {
            timer.cancel();
            return;
        }
    }


    private void drawBar(final Canvas canvas, final Paint paint, final float x, final float width, final float heigth) {
        canvas.drawRect(x, heigth, x + width, canvas.getHeight() - BOTTOM_MARGIN, paint);
    }

    private void drawHVLine(Canvas canvas, Paint paint, int leftMargin, int rightMargin, int bottomMargin, int topMargin) {
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        canvas.drawLine(leftMargin, height - bottomMargin, width - rightMargin, height - bottomMargin, paint);
        canvas.drawLine(leftMargin, topMargin, leftMargin, height - bottomMargin, paint);

    }

    private void drawScale(Canvas canvas, Paint paint, int topMargin, int bottomMargin) {
        int height = canvas.getHeight();
        int top = topMargin + 20;
        int bottom = height - bottomMargin;
        int range = (bottom - top) / 5;
        for (int i = 5; i >= 0; i--) {
            canvas.drawText(String.valueOf(i), 35, top + 4, paint);
            canvas.drawLine(50, top, 70, top, paint);
            top += range;
        }
    }
private void drawAverage(Canvas canvas, int top, int bottom){
        Paint paint = new Paint();
        paint.setARGB(255,255,0,0);
        float y = bottom-(bottom-top)/5f*average;
        canvas.drawLine(50,y,canvas.getWidth(),y,paint);
        canvas.drawText(String.format("%.1f",average),30,y+4,paint);
}
    private float average() {
        int sum = 0;
        for(int i=0;i<records.length;i++){
            sum+=records[i];
        }
        return sum/(float)records.length;
    }
}
